<?php
namespace Tests\Feature\Customer\Inventory;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Helpers\PaginateHelper;
use App\Models\Product\UserInventory;

class CustommerInventoryControllerTest extends TestCase
{
	use RefreshDatabase;
	
	public function testUserInventoriesList()
	{
		$userId		= 5;
		$this->userActingAs([
			'user_type'	=> CUSTOMER_USER,
			'id'		=> $userId
		]);
		
		UserInventory::factory()->count(100)->create([
			'user_id'		=> $userId,
		]);

		$this->get(route('customer.inventory.index', [
				'productType'					=> [1,2],
				PaginateHelper::RECORD_COUNT	=> 50,
			]),
			[
				'Accept'		=> 'application/json',
			]
		)
		->assertOk()
		->assertJsonStructure([
			'data' => [
				'current_page',
				'data'	=> [
					'*'	=> [
						'inventory_id',
						'user_id',
						'product_id',
						'product_count',
						'created_at',
					]
				],
				'total',
				'per_page'
			]
		])
		;
	}
}
