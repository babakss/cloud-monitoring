<?php
namespace Kayer\Monitoring;

use Kayer\Monitoring\APIsInterfaces\HostGroupInterface;
use Kayer\Monitoring\APIsInterfaces\HostInterface;
use Kayer\Monitoring\APIsInterfaces\TemplateInterface;

interface MonitoringInterface
{
	public function hostGroup() : HostGroupInterface;
	
	public function host() : HostInterface;
	
	public function template() : TemplateInterface;
}