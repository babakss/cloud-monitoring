<?php
return [
	'products'	=> [
		SMS =>		[
			'product'	=> SMS,
			'type'		=> COUNTABLE
		],
		EMAIL =>	[
			'product'	=> EMAIL,
			'type'		=> COUNTABLE
		],
		ITEM =>		[
			'product'	=> ITEM,
			'type'		=> COUNTABLE
		],
		TEMPLATE =>	[
			'product'	=> TEMPLATE,
			'type'		=> PERMANENT,
		]
	]
];