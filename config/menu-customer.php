<?php

return [
	'Profile'	=> [
		'title'		=> 'User profile',
		'actions'	=> [
			'index'				=> ['title'	=> 'Get user profile'],
			'show'				=> ['ref'	=> 'index'],
			'update'			=> ['title'	=> 'Update profile'],
			'changePassword'	=> ['title'	=> 'change password'],
		],
	],
	'HostGroup'	=> [
		'title'		=> 'Customer host group',
		'actions'	=> [
			'store'		=> ['title'	=> 'store new host group'],
			'update'	=> ['title'	=> 'update a host group'],
			'delete'	=> ['title'	=> 'delete a host group'],
			'index'		=> ['title'	=> 'Get user host groups'],
			'show'		=> ['ref'	=> 'index'],
			'tree'		=> ['ref'	=> 'index'],
		],
	],
	'Host'	=> [
		'title'		=> 'Customer hosts',
		'actions'	=> [
			'store'		=> ['title'	=> 'store a new host'],
			'update'	=> ['title'	=> 'update a host'],
			'delete'	=> ['title'	=> 'delete a host'],
			'index'		=> ['title'	=> 'Get user host'],
			'show'		=> ['ref'	=> 'index'],
		],
	],
	'Invoice'	=> [
		'title'		=> 'Customer invoices',
		'actions'	=> [
			'store'		=> ['title'	=> 'store an invoice'],
			'update'	=> ['title'	=> 'update an invoice'],
			'delete'	=> ['title'	=> 'delete  an invoice'],
			'index'		=> ['title'	=> 'Get user invoices'],
			'show'		=> ['ref'	=> 'index'],
		],
	],
	'Shopping'	=> [
		'title'		=> 'Customer shopping',
		'actions'	=> [
			'index'		=> ['title'	=> 'shoppings list'],
			'store'		=> ['title'	=> 'store a shopping cart'],
			'buypackage'=> ['title'	=> 'buy a package'],
		],
	],
	'Product'	=> [
		'title'		=> 'Product',
		'actions'	=> [
			'index'		=> ['title'	=> 'product list'],
			'show'		=> ['ref'	=> 'index'],
		],
	],
	'Inventory'	=> [
		'title'		=> 'Inventories',
		'actions'	=> [
			'index'		=> ['title'	=> 'product list']
		],
	],
];