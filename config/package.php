<?php
use App\Models\Package\Package;
return [
	/*
    |--------------------------------------------------------------------------
    | Application Packages Products Category
    |--------------------------------------------------------------------------
    |
    | This value specifies the application supporded packages products category
    |
    */
	'product-status'	=> [
		Package::ACTIVE_PACKAGE,
		Package::INACTIVE_PACKAGE,
	],
	
	'products'	=> [
		SMS =>		[
			'product'	=> SMS,
			'type'		=> COUNTABLE
		],
		EMAIL =>	[
			'product'	=> EMAIL,
			'type'		=> COUNTABLE
		],
		ITEM =>		[
			'product'	=> ITEM,
			'type'		=> COUNTABLE
		],
		TEMPLATE =>	[
			'product'	=> TEMPLATE,
			'type'		=> PERMANENT,
			'model'		=> \App\Models\Template\Template::class
		]
	]
];