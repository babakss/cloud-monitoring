<?php

return [
	'Template'	=> [
		'title'		=> 'Template products',
		'actions'	=> [
			'index'		=> ['title'	=> 'template list'],
			'show'		=> ['ref'	=> 'index'],
			'store'		=> ['title'	=> 'create new template'],
			'update'	=> ['title'	=> 'update a template'],
			'destroy'	=> ['title'	=> 'delete template(s)'],
		],
	],
	'Currency'	=> [
		'title'		=> 'Currency',
		'actions'	=> [
			'index'		=> ['title'	=> 'currency list'],
			'show'		=> ['ref'	=> 'index'],
			'store'		=> ['title'	=> 'create new currency'],
			'update'	=> ['title'	=> 'update a currency'],
			'destroy'	=> ['title'	=> 'delete currency(s)'],
		],
	],
	'Package'	=> [
		'title'		=> 'Package',
		'actions'	=> [
			'index'		=> ['title'	=> 'package list'],
			'show'		=> ['ref'	=> 'index'],
			'store'		=> ['title'	=> 'create new package'],
			'update'	=> ['title'	=> 'update a package'],
			'destroy'	=> ['title'	=> 'delete package(s)'],
		],
	],
	'Invoice'	=> [
		'title'		=> 'Invoice',
		'actions'	=> [
			'pay'		=> ['title'	=> 'pay invoice'],
		],
	],
	'Product'	=> [
		'title'		=> 'Product',
		'actions'	=> [
			'index'		=> ['title'	=> 'product list'],
			'show'		=> ['ref'	=> 'index'],
			'store'		=> ['title'	=> 'create a product'],
			'update'	=> ['title'	=> 'update a product'],
			'destroy'	=> ['title'	=> 'delete product'],
		],
	],
];