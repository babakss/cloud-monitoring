<?php
use App\Http\Controllers\Customer\Inventory\InventoryController;

/*
|--------------------------------------------------------------------------
| Inventory routes
|--------------------------------------------------------------------------
|
|
*/
Route::prefix('customer')->group(function () {
	Route::prefix('inventory')->middleware(['auth:api', 'ACL'])->group(function () {
		Route::get('/', [InventoryController::class, 'index'])
			->name('customer.inventory.index')
			;
	});
});
