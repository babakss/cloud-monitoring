<?php
use App\Http\Controllers\Admin\Package\PackageController;
/*
|--------------------------------------------------------------------------
| Package routes
|--------------------------------------------------------------------------
|
|
*/
Route::prefix('admin')->group(function () {
	Route::prefix('package')->middleware(['auth:api', 'ACL'])->group(function () {
		Route::get('/', [PackageController::class, 'index'])
			->name('admin.package.index')
			;
		Route::get('/{package}', [PackageController::class, 'show'])
			->name('admin.package.show')
			;
		Route::post('/', [PackageController::class, 'store'])
			->name('admin.package.create')
			;
		Route::put('/{package}', [PackageController::class, 'update'])
			->where('package', '[0-9]+')
			->name('admin.package.update')
			;
		Route::delete('/{package}', [PackageController::class, 'destroy'])
			->where('package', '[0-9]+')
			->name('admin.package.destroy')
			;
	});
});