<?php
use App\Helpers\Str;
use App\Http\Controllers\Admin\Invoice\InvoiceController;
use App\Http\Controllers\Customer\Invoice\InvoiceController as CustomerInvoiceController;

/*
|--------------------------------------------------------------------------
| Invoice routes
|--------------------------------------------------------------------------
|
|
*/
Route::prefix('admin')->group(function () {
	Route::prefix('invoice')->middleware(['auth:api', 'ACL'])->group(function () {
		Route::post('/pay/{payType}/{invoice}', [InvoiceController::class, 'pay'])
			->name('admin.invoice.pay')
			->where([
				'invoice'	=> '[0-9]+',
				'payType'	=> '('. Str::implode('|', config('payment.pay_types')).')'
			])
			;
	});
});

Route::prefix('customer')->group(function () {
	Route::prefix('invoice')->middleware(['auth:api', 'ACL'])->group(function () {
		Route::get('/', [CustomerInvoiceController::class, 'index'])
			->name('customer.invoice.index')
			;
		Route::get('/{invoice}', [CustomerInvoiceController::class, 'show'])
			->where('invoice', '[0-9]+')
			->name('customer.invoice.show')
			;
		Route::post('/', [CustomerInvoiceController::class, 'store'])
			->name('customer.invoice.store')
			;
		Route::put('/{invoice}', [CustomerInvoiceController::class, 'update'])
			->where('invoice', '[0-9]+')
			->name('customer.invoice.update')
			;
		Route::delete('/{invoice}', [CustomerInvoiceController::class, 'delete'])
			->where('invoice', '[0-9]+')
			->name('customer.invoice.delete')
			;
	});
});
