<?php
use App\Http\Controllers\Admin\Currency\CurrencyController;
/*
|--------------------------------------------------------------------------
| Currency routes
|--------------------------------------------------------------------------
|
|
*/
Route::prefix('admin')->group(function () {
	Route::prefix('currency')->middleware(['auth:api', 'ACL'])->group(function () {
		Route::get('/', [CurrencyController::class, 'index'])
			->name('admin.currency.index')
			;
		Route::get('/{currency}', [CurrencyController::class, 'show'])
			->name('admin.currency.show')
			;
		Route::post('/', [CurrencyController::class, 'store'])
			->name('admin.currency.create')
			;
		Route::put('/{currency}', [CurrencyController::class, 'update'])
			->where('currency', '[0-9]+')
			->name('admin.currency.update')
			;
		Route::delete('/{currency}', [CurrencyController::class, 'destroy'])
			->where('currency', '[0-9]+')
			->name('admin.currency.destroy')
			;
	});
});