<?php
use App\Http\Controllers\Customer\User\UserController;
use App\Http\Controllers\Customer\User\ProfileController;
/*
|--------------------------------------------------------------------------
| Customer User Routes
|--------------------------------------------------------------------------
|
|
*/
Route::prefix('customer')->group(function () {
	Route::prefix('user')->group(function () {
		Route::post('/', [UserController::class, 'store'])
			->name('customer.user.store')
		;
		Route::post('/verify/{user}', [UserController::class, 'verify'])
			->where('user', '[0-9]+')
			->name('customer.user.verify.cellphone')
		;
		Route::post('/resend-verify-code/{user}', [UserController::class, 'resendVerifyCode'])
			->where('user', '[0-9]+')
			->name('customer.user.resend.verifycode')
		;
	});
	
	Route::prefix('user')->middleware(['auth:api', 'ACL'])->group(function () {
		Route::get('/', [ProfileController::class, 'show'])
			->name('customer.profile.show')
			;
		Route::put('/', [ProfileController::class, 'update'])
			->name('customer.profile.update')
		;
		Route::patch('/changePass', [ProfileController::class, 'changePassword'])
			->name('customer.profile.changePassword')
		;
	});
});