<?php
use App\Http\Controllers\Admin\Product\ProductController;
/*
|--------------------------------------------------------------------------
| Products routes
|--------------------------------------------------------------------------
|
|
*/

Route::prefix('admin')->group(function () {
	Route::prefix('product')->middleware(['auth:api', 'ACL'])->group(function () {
		Route::get('/', [ProductController::class, 'index'])
			->name('admin.product.index')
			;
		
		Route::get('/{product}', [ProductController::class, 'show'])
			->name('admin.product.show')
			->where('product', '[0-9]+')
			;
		
		Route::post('/', [ProductController::class, 'store'])
			->name('admin.product.store')
			;
		
		Route::put('/{product}', [ProductController::class, 'update'])
			->name('admin.product.update')
			->where('product', '[0-9]+')
			;
		
		Route::delete('/{product}', [ProductController::class, 'destroy'])
			->name('admin.product.delete')
			->where('product', '[0-9]+')
			;
	});
});

Route::prefix('customer')->group(function () {
	Route::prefix('product')->middleware(['auth:api', 'ACL'])->group(function () {
		Route::get('/', [ProductController::class, 'index'])
			->name('customer.product.index')
			;
		
		Route::get('/{product}', [ProductController::class, 'show'])
			->name('customer.product.show')
			->where('product', '[0-9]+')
			;
	});
});