<?php
use App\Http\Controllers\Customer\Host\HostController;
/*
|--------------------------------------------------------------------------
| Hosts Routes
|--------------------------------------------------------------------------
|
|
*/
Route::prefix('customer')->group(function () {
	Route::prefix('host')->middleware(['auth:api', 'ACL'])->group(function () {
		Route::get('/', [HostController::class, 'index'])
			->name('customer.host.index')
		;

		Route::post('/', [HostController::class, 'store'])
			->name('customer.host.store')
		;

		Route::put('/{hostObject}', [HostController::class, 'update'])
			->where('hostObject', '[0-9]+')
			->name('customer.host.update')
			;

		Route::delete('/{hostObject}', [HostController::class, 'delete'])
			->where('hostObject', '[0-9]+')
			->name('customer.host.delete')
			;
	});
});