<?php
use App\Http\Controllers\Customer\Shopping\ShoppingController;
/*
|--------------------------------------------------------------------------
| Shopping routes
|--------------------------------------------------------------------------
|
|
*/

Route::prefix('customer')->group(function () {
	Route::prefix('shopping')->middleware(['auth:api', 'ACL'])->group(function () {
		Route::get('/', [ShoppingController::class, 'index'])
			->name('customer.shopping.index')
			;
		Route::post('/', [ShoppingController::class, 'store'])
			->name('customer.shopping.store')
			;
		Route::post('/{package}', [ShoppingController::class, 'buypackage'])
			->where('package', '[0-9]+')
			->name('customer.shopping.buypackage')
			;
	});
});