<?php
use App\Http\Controllers\Customer\HostGroup\HostGroupController;
/*
|--------------------------------------------------------------------------
| Hosts group Routes
|--------------------------------------------------------------------------
|
|
*/
Route::prefix('customer')->group(function () {
	Route::prefix('host-groups')->middleware(['auth:api', 'ACL'])->group(function () {
		Route::get('/', [HostGroupController::class, 'index'])
			->name('customer.hostgroup.index')
			;
		
		Route::get('/tree', [HostGroupController::class, 'tree'])
			->name('customer.hostgroup.tree')
			;
		
		Route::get('/{group}', [HostGroupController::class, 'show'])
			->where('group', '[0-9]+')
			->name('customer.hostgroup.show')
			;
		
		Route::post('/', [HostGroupController::class, 'store'])
			->name('customer.hostgroup.store')
			;
	
		Route::put('/{group}', [HostGroupController::class, 'update'])
			->where('group', '[0-9]+')
			->name('customer.hostgroup.update')
			;
		
		Route::delete('/{group}', [HostGroupController::class, 'delete'])
			->where('group', '[0-9]+')
			->name('customer.hostgroup.delete')
			;
	});
});