<?php

namespace App\Models\Package;

use App\Base\BaseModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Package extends BaseModel
{
	use HasFactory;
	
	const ACTIVE_PACKAGE	= 1; 
	const INACTIVE_PACKAGE	= 2; 
	protected $table		= 'packages';
	protected $primaryKey	= 'package_id';
	
	protected $fillable	= [
		'title',
		'description',
		'price',
		'status',
	];
	
	public function packageItems()
	{
		return $this->hasMany(PackageItems::class, 'package_id', 'package_id');
	}
}
