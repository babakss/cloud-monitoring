<?php
namespace App\Http\Controllers\Customer\Inventory;

use Exception;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\ControllerAbstract;
use App\Http\Requests\Customer\Inventory\IndexInventoryRequest;
use App\Helpers\PaginateHelper;
use App\Models\Product\UserInventory;
use Illuminate\Support\Facades\DB;

class InventoryController extends ControllerAbstract
{
	
	public function index(IndexInventoryRequest $request)
	{
		try {
			$fetchParams	= $request->validated();
			return $this->getResponse(
				UserInventory::userInventories($request->user()->id)->fetchData($fetchParams)
					->paginate($fetchParams[PaginateHelper::RECORD_COUNT])
				);
		} catch (Exception $e) {
			return $this->getResponse([
				self::EXCEPTION_MESSAGE	=> $e->getMessage(),
			], Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}