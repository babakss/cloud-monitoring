<?php

namespace App\Http\Controllers\Admin\Package;

use Exception;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\ControllerAbstract;
use App\Models\Package\Package;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\Admin\Package\CreatePackageRequest;
use App\Http\Requests\Admin\Package\UpdatePackageRequest;
use App\Http\Requests\Admin\Package\DeletePackageRequest;
use App\Helpers\AuditLog\AuditLogAbstract;

class PackageController extends ControllerAbstract
{
	/*
	 *  Create New Template (Only By Admin )
	 */
    public function store(CreatePackageRequest $request) 
	{
		DB::beginTransaction();
		try {
			$product	= config('package.products');
			$data		= $request->validated();
			foreach ($data['product_items'] as $key => $value){
				if($product[$value['product_id']]['type'] == PERMANENT)
				{
					$data['product_items'][$key]['count'] = 1;
				}
			}
			$package	= new Package($data);
			$package->save();
			$package->packageItems()->createMany($data['product_items']);
			$package->load('packageItems');
			$this->saveAuditLog(
				$package->getKey(),
				AuditLogAbstract::PACKAGE_RESOURCE,
				AuditLogAbstract::INSERT_ACTION,
				$package->toArray(),
				$package->title
			);
			DB::commit();
			return $this->getResponse([
				'id'	=> $package->getKey()
			], Response::HTTP_CREATED);
		} catch (Exception $ex) {
			DB::rollBack();
			return $this->getResponse([
				self::EXCEPTION_MESSAGE => $ex->getMessage()
			], Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
	
	/*
	 *  Update Templates (Only By Admin )
	 */
	
	public function update(UpdatePackageRequest $request,Package $package)
	{
		DB::beginTransaction();
		try {
			$product	= config('package.products');
			$package->load('packageItems');
			$oldData	= $package->toArray();
			$data		= $request->validated();
			$package->update($data);
			foreach ($data['product_items'] as $key => $value){
				if($product[$value['product_id']]['type'] == PERMANENT)
				{
					$data['product_items'][$key]['count'] = 1;
				}
			}
			if(!empty($data['product_items'])) {
				$package->packageItems()->delete();
				$package->packageItems()->createMany($data['product_items']);
			}
			$package->load('packageItems');

			//Save audit log
			$this->saveAuditLog(
				$package->getKey(),
				AuditLogAbstract::PACKAGE_RESOURCE,
				AuditLogAbstract::UPDATE_ACTION,
				AuditLogAbstract::getDifference($oldData, $package->toArray(), [
//					'sub'	=> [
//						'product_items'	=> [
//							'key'	=> [
//								'resource',
//								'action'
//							],
//							'check'	=> [
//								'params'
//							]
//						]
//					]
				]),
				$package->title
			);
			DB::commit();
			return $this->getResponse([]);
		} catch (Exception $ex) {
			return $this->getResponse([
				self::EXCEPTION_MESSAGE => $ex->getMessage()
			], Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
	
	/*
	 *  Delete Templates (Only By Admin )
	 */
	
	public function destroy(DeletePackageRequest $request,Package $package)
	{
		//@TODO check if a user has this template it can not be deleted 
		try {
			$package->delete($package);
			return $this->getResponse([], Response::HTTP_OK);
		} catch (Exception $ex) {
			return $this->getResponse([
				self::EXCEPTION_MESSAGE => $ex->getMessage()
			], Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
	
	/*
	 *  Show Specific Template Accourding To Template Id 
	 */
	
	public function show(Package $package)
	{
		try {
			$package->load('packageItems');
			return $this->getResponse($package);
		} catch (Exception $ex) {
			return $this->getResponse([
				self::EXCEPTION_MESSAGE => $ex->getMessage()
			], Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
