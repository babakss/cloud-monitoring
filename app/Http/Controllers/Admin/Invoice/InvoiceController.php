<?php
namespace App\Http\Controllers\Admin\Invoice;

use Exception;
use App\Exceptions\PaymentVerificationException;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\ControllerAbstract;
use App\Models\Invoice\Invoice;
use Illuminate\Support\Facades\DB;
use App\Helpers\AuditLog\AuditLogAbstract;
use App\Models\Bank\Bank;
use App\Helpers\PaymentHelper;

use App\Helpers\Payment\PaymentRequestInterface;

class InvoiceController extends ControllerAbstract
{
	public function __construct()
	{
		//Inject payment class based on pay type parameter
		app()->bind(PaymentRequestInterface::class, 'App\Helpers\Payment\Requests\\'.request()->payType.'Request');
	}
	
	public function pay(PaymentRequestInterface $request, $payType, Invoice $invoice)
	{
		DB::beginTransaction();
		try {
			PaymentHelper::pay($request, $invoice, $request->validated());
			$this->saveAuditLog(
				$invoice->getKey(),
				AuditLogAbstract::BANK_RESOURCE,
				AuditLogAbstract::UPDATE_ACTION,
				$invoice->toArray(),
				$invoice->amount
			);
			DB::commit();
			return $this->getResponse([], Response::HTTP_OK);
		} catch (PaymentVerificationException $e) {
			return $this->getResponse([], Response::HTTP_NOT_ACCEPTABLE);
		} catch (Exception $e) {
			DB::rollBack();
			return $this->getResponse([
				self::EXCEPTION_MESSAGE	=> $e->getMessage(),
			], Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}