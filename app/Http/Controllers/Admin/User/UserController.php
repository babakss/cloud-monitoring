<?php
namespace App\Http\Controllers\Admin\User;

use Exception;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\ControllerAbstract;
use App\Http\Requests\Admin\User\CreateUserRequest;
use App\Http\Requests\Department\UpdateDepartmentRequest;


class UserController extends ControllerAbstract
{
	/**
	 * Store new user
	 * @param CreateDepartmentRequest $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(CreateUserRequest $request)
	{
		try {
			$data				= $request->validated();
			$data['user_type']	= ADMIN_USER;
			$user				= new User($data);
			$user->password		= Hash::make($data['password']);
			$user->save();
			return $this->getResponse([
				'id'	=> $user->getKey()
			], Response::HTTP_CREATED);
		} catch (Exception $ex) {
			return $this->getResponse([
				self::EXCEPTION_MESSAGE => $ex->getMessage()
			], Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}