<?php
namespace App\Http\Requests\Customer\Invoice;

use App\Base\BaseRequest;

class CreateInvoiceRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'amount'		=> 'required|numeric|min:0',
			'description'	=> 'nullable|string',
		];
	}
}
